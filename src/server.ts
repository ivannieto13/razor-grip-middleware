import { http } from './app';


http.listen(process.env.PORT || 3000, (err?: Error) => {
  if (err) {
    return console.error(err);
  }
  return console.log(`server is listening on 3000`);
});
