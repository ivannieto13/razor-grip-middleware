import { createHash } from 'crypto';
import { STRING, NOW, DATE } from 'sequelize';
import { db } from '../../db/pg';

export class Room {
  constructor(participants: string[]) {
    this.participants = participants;
  }
  private participants: string[];
  public room_id: string;

  public async createRoom() {
    try {
      const room = await this.roomEntity().create({
        room_id: this.room_id,
        participant_1: this.participants[0],
        participant_2: this.participants[1],
      });
      return { code: 201, data: room };
    } catch (error) {
      if(error.errors[0].validatorKey === 'not_unique') {
        return { code: 200, data: { room_id: error.errors[0].value }};
      }
      return { code: 500, data: error };
    }
  }

  public getRoomId() {
    this.participants = this.participants.sort((a, b) => a.localeCompare(b));
    this.room_id = createHash('sha256').update(this.participants.toString()).digest('hex');
    return this.room_id;
  }

  public roomEntity() {
    return db.define('room', {
      room_id: {
        type: STRING,
        primaryKey: true,
      },
      participant_1: {
        type: STRING,
      },
      participant_2: {
        type: STRING,
      },
      created_at: {
        type: DATE,
        defaultValue: NOW,
      },
      updated_at: {
        type: DATE,
        defaultValue: NOW,
      }
    }, {
      tableName: 'rooms',
      schema: 'sn-schema',
      underscored: true
    });
  }
}