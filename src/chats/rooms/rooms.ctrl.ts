import { Request, Response } from 'express';
import { Room } from './rooms.model';

class ChatsCtrl {
  public async newRoom(req: Request, res: Response): Promise<void> {
    try {
      const { to } = req.params;
      const uid = req.headers.uid;
      const room = new Room([to, String(uid)]);
      room.getRoomId();
      const create = await room.createRoom();
      res.status(create.code).json(create.data);
    } catch (error) { 
      res.status(500).json({ error});
    }
  }
  
  
}

export default new ChatsCtrl();
