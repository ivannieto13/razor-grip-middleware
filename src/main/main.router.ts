import { Router } from 'express';
import { verifyToken } from '../auth/jwt';
import { chatsRouter } from '../chats/chats.router';
import { usersRouter } from '../users/users.router';

export const mainRouter: Router = Router();

mainRouter
  .use(verifyToken)
  .use('/chats', chatsRouter)
  .use('/users', usersRouter)
  ;
